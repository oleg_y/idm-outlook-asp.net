﻿using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using IDM_Outlook.Models;
using System.Net;
using System.Text;
using System.IO;
using System.Xml.Linq;

namespace IDM_Outlook.Controllers
{
    public class HomeController : Controller
    {
        private const string GetAttachmentSoapRequest =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
            xmlns:xsd=""http://www.w3.org/2001/XMLSchema""
            xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/""
            xmlns:t=""http://schemas.microsoft.com/exchange/services/2006/types"">
            <soap:Header>
            <t:RequestServerVersion Version=""Exchange2013"" />
            </soap:Header>
              <soap:Body>
                <GetAttachment xmlns=""http://schemas.microsoft.com/exchange/services/2006/messages""
                xmlns:t=""http://schemas.microsoft.com/exchange/services/2006/types"">
                  <AttachmentShape/>
                  <AttachmentIds>
                    <t:AttachmentId Id=""{0}""/>
                  </AttachmentIds>
                </GetAttachment>
              </soap:Body>
            </soap:Envelope>";

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Dialog()
        {
            return View();
        }

        public IActionResult AuthRedirect()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost("api/attachment")]
        public IActionResult RetrieveAttachmentFromEWS([FromBody] AttachmentRequestModel request)
        {
            HttpWebRequest webRequest = WebRequest.CreateHttp(request.EwsUrl);
            webRequest.Headers.Add("Authorization",
              string.Format("Bearer {0}", request.AttachmentToken));
            webRequest.PreAuthenticate = true;
            webRequest.AllowAutoRedirect = false;
            webRequest.Method = "POST";
            webRequest.ContentType = "text/xml; charset=utf-8";

            // Construct the SOAP message for the GetAttachment operation.
            byte[] bodyBytes = Encoding.UTF8.GetBytes(
              string.Format(GetAttachmentSoapRequest, request.AttachmentId));
            webRequest.ContentLength = bodyBytes.Length;

            Stream requestStream = webRequest.GetRequestStream();
            requestStream.Write(bodyBytes, 0, bodyBytes.Length);
            requestStream.Close();

            // Make the request to the Exchange server and get the response.
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "POST");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization");

            // If the response is okay, create an XML document from the reponse
            // and process the request.
            if (webResponse.StatusCode == HttpStatusCode.OK)
            {
                var responseStream = webResponse.GetResponseStream();
                var responseEnvelope = XElement.Load(webResponse.GetResponseStream());

                responseStream.Close();
                webResponse.Close();

                if (responseEnvelope != null)
                {
                    var fileAttachment = responseEnvelope.Descendants().Where(descendant => descendant.Name.LocalName == "FileAttachment");
                    if (fileAttachment.Count() > 0)
                    {
                        AttachmentResponseModel response = new AttachmentResponseModel();
                        response.Name = request.AttachmentName;
                        response.Content = fileAttachment.Descendants().Where(content => content.Name.LocalName == "Content").FirstOrDefault().Value;
                        return Ok(response);
                    }
                    
                }
            }
            return NotFound(string.Format("Attachment \"{0}\" was not found. " , request.AttachmentName));
        }
    }
}
