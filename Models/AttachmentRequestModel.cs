﻿namespace IDM_Outlook.Models
{
    public class AttachmentRequestModel
    {
        public string AttachmentToken { get; set; }
        public string EwsUrl { get; set; }
        public string AttachmentName { get; set; }
        public string AttachmentId { get; set; }
    }
}
