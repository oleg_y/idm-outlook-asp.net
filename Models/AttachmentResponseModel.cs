﻿namespace IDM_Outlook.Models
{
    public class AttachmentResponseModel
    {
        public string Name { get; set; }
        public string Content { get; set; }
    }
}