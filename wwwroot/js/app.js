// https://docs.microsoft.com/en-us/outlook/add-ins
let mainApp = angular.module("mainApp", ['ngRoute', 'moment-picker']);

mainApp.config(function ($routeProvider, $locationProvider) {
  $locationProvider.hashPrefix('');
  $routeProvider
      .when('/document', {
        templateUrl: 'js/Templates/document.html',
        controller: 'documentController'
      })
      .when('/settings', {
        templateUrl: 'js/Templates/settings.html',
        controller: 'settingsController'
      })
      .when('/help', {
        templateUrl: 'js/Templates/help.html',
        controller: 'helpController'
      })
      .otherwise({
        redirectTo: '/document'
      });
});

mainApp.directive('onReadFile', function ($parse) {
  return {
    restrict: 'A',
    scope: false,
    link: function (scope, element, attr) {
      // noinspection JSUnresolvedVariable
      let fn = $parse(attr.onReadFile);

      element.on('change', function (onChangeEvent) {

        let fileExt = onChangeEvent.target.files[0].name;
        fileExt = fileExt.substring(fileExt.lastIndexOf('.'));

        let reader = new FileReader();

        reader.onload = function (onLoadEvent) {
          scope.$apply(function () {
            if (fileExt === ".ionapi" || fileExt === ".IONAPI") {
              // noinspection JSUnresolvedVariable
              fn(scope, {$fileContent: onLoadEvent.target.result});
            } else {
              let obj = {"status": "error"};
              fn(scope, {$fileContent: JSON.stringify(obj)});
            }
          });
        };
        reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
      });
    }
  };
});

Office.initialize = function () {
  let container = document.getElementById('container');
  angular.bootstrap(container, ['mainApp'], {"strictDi": false});
};
