mainApp.controller('documentController', ['$scope', '$http', '$q', '$window', '$log', 'ProfileService', 'StringService',
  'ConstantService', 'AttributeService', 'DocumentService', 'AttachmentsService', 'URIService', 'ItemService',
  function ($scope, $http, $q, $window, $log, ProfileService, StringService, ConstantService,
            AttributeService, DocumentService, AttachmentsService, URIService, ItemService) {

    // Error statuses
    $scope.isAuthorized = false;
    $scope.hasProfiles = false;
    $scope.hasAttachments = false;

    // State flags
    $scope.isLoading = false;
    $scope.isError = false;
    $scope.isFormValid = true;
    $scope.showFormControls = false;
    $scope.isAclSelected = false;

    // Data
    $scope.documentTypes = [];
    $scope.attachments = [];
    $scope.attributes = [];
    $scope.acls = [];
    $scope.selectedAclDescription = null;
    $scope.selectedAttachment = null;
    $scope.aclNotSelectedError = '';
    $scope.selectedAttachmentId = '';

    const refreshProfile = function () {
      const activeProfile = ProfileService.getActiveProfile();
      const request = {
        method: URIService.HTTPMethods.POST,
        url: activeProfile[ConstantService.ION_OBJECT_KEY][ConstantService.ION_REFRESH_HOST_KEY] +
        activeProfile[ConstantService.ION_OBJECT_KEY][ConstantService.ION_REFRESH_PATH_KEY],
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        data: {
          client_secret: activeProfile[ConstantService.ION_OBJECT_KEY][ConstantService.ION_SECRET_KEY],
          client_id: activeProfile[ConstantService.ION_OBJECT_KEY][ConstantService.ION_CLIENT_ID_KEY],
          grant_type: "refresh_token",
          refresh_token: activeProfile[ConstantService.TOKEN_KEY]
        }
      };
      $http(request).then(function (response) {
        $scope.isLoading = false;
        console.log("Refreshing token response: ", response);
      }, function (error) {
        $scope.isLoading = false;
        console.log('Token refresh error: ', error);
        if (error.status === URIService.HTTPStatuses.INVALID_CLIENT) {
          $scope.showNotification(
            StringService.getValue(ConstantService.CONFIGURATION_ERROR_KEY),
            StringService.getValue(ConstantService.CONNECT_PROFILE_KEY));
        }
        URIService.redirectToSettings();
      });
    };

    const loadDocumentTypes = function (activeProfile) {
      $scope.isLoading = true;
      const request = URIService.getDocumentTypesRequest(activeProfile);
      $http(request).then(function (response) {
        $scope.isLoading = false;
        $scope.documentTypes = DocumentService.filterDocumentTypesLength(response.data.entities.entity);
        DocumentService.setDocumentTypes(response.data.entities.entity);
      }, function (error) {
        if (error.status === URIService.HTTPStatuses.UNAUTHORIZED) {
          return refreshProfile();
        }
        if (error.status === URIService.HTTPStatuses.SERVER_ERROR) {
          $scope.showNotification(StringService.getValue(ConstantService.ERROR_KEY),
            StringService.getValue(ConstantService.SERVER_ERROR_KEY));
        }
        if (error.status === URIService.HTTPStatuses.UNKNOWN) {
          $scope.showNotification(StringService.getValue(ConstantService.ERROR_KEY),
            StringService.getValue(ConstantService.CONNECT_PROFILE_KEY));
        }
        URIService.redirectToSettings();
      });
    };

    const loadActiveProfile = function () {
      const activeProfile = ProfileService.getActiveProfile();
      if (activeProfile[ConstantService.ION_OBJECT_KEY] === undefined) {
        return $scope.showNotification(
          StringService.getValue(ConstantService.CONFIGURATION_ERROR_KEY,
            StringService.getValue(ConstantService.ADD_PROFILE_KEY))
        );
      }
      if (activeProfile[ConstantService.TOKEN_KEY] === undefined || activeProfile[ConstantService.TOKEN_KEY] === null) {
        return $scope.showNotification(
          StringService.getValue(ConstantService.CONFIGURATION_ERROR_KEY,
            StringService.getValue(ConstantService.CONNECT_PROFILE_KEY))
        );
      }
      loadDocumentTypes(activeProfile);
    };

    const validateAttributes = function () {
      $scope.attributes.forEach(function (attribute) {
        $scope.validateAttribute(attribute);
      });
      if (!$scope.isAclSelected) {
        $scope.aclNotSelectedError = StringService.getValue(ConstantService.SELECT_ACL_KEY);
      }
    };

    $scope.validateAttribute = function (attribute) {
      if (!AttributeService.isAttributeRequired(attribute)
        && AttributeService.isAttributeValueEmpty(attribute, 'value')) {
        attribute.valid = true;
        return;
      }
      if (AttributeService.isAttributeRequired(attribute)
        && AttributeService.isAttributeValueEmpty(attribute, 'value')) {
        attribute.valid = false;
        attribute.validationError = StringService.getValue(ConstantService.REQUIRED_FIELD_KEY);
        return;
      }
      if (AttributeService.isAttributeTypeLimitedString(attribute)) {
        AttributeService.validateString(attribute);
        return;
      }

      switch (attribute.type) {
        case AttributeService.ATTRIBUTE_TYPES.BOOLEAN:
          AttributeService.validateBoolean(attribute);
          console.log('Validating bool: ', attribute);
          break;
        case AttributeService.ATTRIBUTE_TYPES.DATE:
          console.log('Validating date: ', attribute);
          AttributeService.validateDate(attribute);
          break;
        case AttributeService.ATTRIBUTE_TYPES.TIME:
          console.log('Validating time: ', attribute);
          AttributeService.validateTime(attribute);
          break;
        case AttributeService.ATTRIBUTE_TYPES.TIMESTAMP:
          console.log('Validating timestamp: ', attribute);
          AttributeService.validateTimestamp(attribute);
          break;
        case AttributeService.ATTRIBUTE_TYPES.SHORT:
        case AttributeService.ATTRIBUTE_TYPES.LONG:
        case AttributeService.ATTRIBUTE_TYPES.DECIMAL:
        case AttributeService.ATTRIBUTE_TYPES.DOUBLE:
          console.log('Validating number: ', attribute);
          AttributeService.validateNumber(attribute);
          break;
        case AttributeService.ATTRIBUTE_TYPES.STRING:
          // currently not validated
          attribute.valid = true;
          break;
        default:
          console.log('Default, attribute not validated: ', attribute);
      }
    };

    const isFormValid = function () {
      const attributesValid = $scope.attributes.every(function (attribute) {
        if ('valid' in attribute) {
          return attribute.valid === true;
        }
        return true;
      });
      return attributesValid && $scope.isAclSelected;
    };

    $scope.init = function () {
      const profiles = ProfileService.getProfiles();
      if (profiles.length === 0) {
        $scope.hasProfiles = false;
        $scope.showNotification(
          StringService.getValue(ConstantService.CONFIGURATION_ERROR_KEY),
          StringService.getValue(ConstantService.PROFILES_EMPTY_KEY));
        URIService.redirectToSettings();
        return;
      }

      $scope.hasProfiles = true;
      const attachments = AttachmentsService.getAttachments();

      if (attachments.length > 0) {
        const selectedAttachment = attachments[0];
        $scope.hasAttachments = true;
        $scope.attachments = AttachmentsService.filterAttachmentsNameLength(attachments);
        $scope.selectedAttachmentId = selectedAttachment.id;
        AttachmentsService.setSelectedAttachment(selectedAttachment);
        loadActiveProfile();
      } else {
        $scope.hasAttachments = false;
        $scope.errorMessage = StringService.getValue(ConstantService.ATTACHMENTS_EMPTY_ERROR);
      }
    };

    $scope.updateAttributesAndAcl = function (selectedDocumentTypeName) {
      const selectedDocumentTypes = $scope.documentTypes.filter(function (documentType) {
        return documentType.name === selectedDocumentTypeName;
      });
      if (selectedDocumentTypes.length > 0) {
        const selectedDocumentType = selectedDocumentTypes[0];
        $scope.attributes = selectedDocumentType.attrs.attr;
        $scope.acls = selectedDocumentType.acls.acl;
        $scope.showFormControls = true;
        DocumentService.setSelectedACLs($scope.acls);
        DocumentService.setSelectedDocumentType(selectedDocumentType);
      }
    };

    $scope.updateSelectedAcl = function (selectedAclDescription) {
      const selectedAcls = $scope.acls.filter(function (acl) {
        return acl.name === selectedAclDescription;
      });
      if (selectedAcls.length > 0) {
        const selectedACL = selectedAcls[0];
        $scope.isAclSelected = true;
        $scope.selectedAclDescription = selectedACL.desc;
        DocumentService.setSelectedACLDescription(selectedACL.desc);
        DocumentService.setSelectedACLName(selectedACL.name);
      } else {
        $scope.isAclSelected = false;
      }
    };

    $scope.isAttributeRequired = function (attribute) {
      return AttributeService.isAttributeRequired(attribute);
    };

    $scope.isAttributeTypeBoolean = function (attribute) {
      return AttributeService.isAttributeTypeBoolean(attribute);
    };

    $scope.isAttributeTypeValueSet = function (attribute) {
      return AttributeService.isAttributeTypeValueSet(attribute);
    };

    $scope.isAttributeInputEditable = function (attribute) {
      return !$scope.isAttributeTypeValueSet(attribute) && !$scope.isAttributeTypeBoolean(attribute);
    };

    $scope.isAttributeOfDateType = function (attribute) {
      return AttributeService.isAttributeTypeDate(attribute)
        || AttributeService.isAttributeTypeTime(attribute)
        || AttributeService.isAttributeTypeTimestamp(attribute);
    };

    $scope.isAttributeTypeTimestamp = function (attribute) {
      return AttributeService.isAttributeTypeTimestamp(attribute);
    };

    $scope.isAttributeTypeDate = function (attribute) {
      return AttributeService.isAttributeTypeDate(attribute);
    };

    $scope.isAttributeTypeTime = function (attribute) {
      return AttributeService.isAttributeTypeTime(attribute);
    };

    $scope.getAttributeValueSets = function (attribute) {
      if ($scope.isAttributeTypeValueSet(attribute)) {
        // noinspection JSUnresolvedVariable
        return attribute.valueset.value;
      }
    };

    $scope.selectAttachment = function (attachment) {
      $scope.selectedAttachmentId = attachment.id;
      AttachmentsService.setSelectedAttachment(attachment);
    };

    $scope.shareAttachment = function () {
      validateAttributes();
      if (isFormValid()) {
        $scope.isLoading = true;
        ItemService.getEWSToken(function (tokenResponse) {
          if (tokenResponse.status === 'succeeded') {
            ItemService.retrieveAttachment(tokenResponse.value)
              .then(function (attachment) {
                ItemService.postToIDM(attachment.data)
                  .then(function () {
                    $scope.isLoading = false;
                    $scope.showNotification("Success", 'Document was uploaded successfully.');
                    AttributeService.resetAttributes($scope.attributes);
                  }, function (error) {
                    $scope.isLoading = false;
                    const idmError = error.data.error;
                    const errorString = idmError.message + ', Detail: ' + idmError.detail;
                    $log.debug(errorString);
                    $scope.showNotification('Error', errorString);
                  });
              }, function (error) {
                $scope.isLoading = false;
                $log.debug(error.message);
                $scope.showNotification('Error', 'Server error, please try again later.');
              });
          } else {
            // Show token error, when reproduced.
            $scope.isLoading = false;
          }
        });
      }
    };
    $scope.init();
  }]);
