mainApp.controller('settingsController', ['$scope', '$window', '$http', '$location', '$timeout', 'ProfileService', 'ConstantService', 'StringService', 'URIService',
  function ($scope, $window, $http, $location, $timeout, ProfileService, ConstantService, StringService, URIService) {

    $scope.ionFile = null;
    $scope.dialog = {};
    $scope.profileName = '';
    $scope.profiles = [];
    $scope.isBusy = false;

    $scope.init = function () {
      $scope.profiles = ProfileService.getProfiles();
    };

    $scope.hasProfiles = function () {
      return $scope.profiles.length > 0;
    };

    $scope.removeProfile = function (profile) {
      ProfileService.removeProfile(profile);
      $scope.profiles = ProfileService.getProfiles();
      $scope.showNotification(StringService.getValue(ConstantService.SUCCESS_KEY),
        StringService.getValue(ConstantService.PROFILE_REMOVED_KEY, profile.name));
    };

    $scope.redirectMingle = function () {
      if ($scope.content === undefined) {
        return $scope.showNotification(StringService.getValue(ConstantService.ERROR_KEY),
          StringService.getValue(ConstantService.UPLOAD_ION_FILE_KEY));
      }
      if ($scope.profileName === undefined || $scope.profileName === "") {
        return $scope.showNotification(StringService.getValue(ConstantService.ERROR_KEY),
          StringService.getValue(ConstantService.PROFILE_NAME_EMPTY_KEY));
      }
      $scope.getDialog();
    };

    /**
     * Invoke auth dialog window in order to refresh credentials
     * @param profile
     */
    $scope.refreshProfile = function (profile) {
      $scope.ionFile = profile[ConstantService.ION_OBJECT_KEY];
      ProfileService.setProfile(profile);
      $scope.getDialog();
    };

    $scope.getDialog = function () {
      if (!Office.context.requirements.isSetSupported(ConstantService.MAILBOX, ConstantService.OUTLOOK_MIN_VERSION)) {
        $scope.showNotification(StringService.getValue(ConstantService.NOT_SUPPORTED_KEY),
          StringService.getValue(ConstantService.UPDATE_OUTLOOK_KEY));
        return;
      }
      $scope.isBusy = true;
      const dialogCallback = function (asyncResult) {
        if (asyncResult.status === ConstantService.ASYNC_RESULT_FAILED) {
          return dialogDeny();
        }
        $scope.dialog = asyncResult.value;
        $scope.dialog.addEventHandler(
          Office.EventType.DialogEventReceived, processDialogEvent);
        // noinspection JSUnresolvedVariable
        $scope.dialog.addEventHandler(
          Microsoft.Office.WebExtension.EventType.DialogMessageReceived, processAuthenticationDialogResponse);
      };
      Office.context.ui.displayDialogAsync(URIService.getDialogWindowUrl(), {height: 50, width: 50}, dialogCallback);
    };

    function dialogDeny() {
      try {
        $scope.dialog.close();
      } catch (e) {
        // pass, no action
      }
      $timeout(function () {
        $scope.isBusy = false;
      });
    }

    function processDialogEvent(arg) {
      // https://docs.microsoft.com/en-us/office/dev/add-ins/develop/dialog-api-in-office-add-ins#handle-errors-and-events
      switch (arg.error) {
        case 12002:
          /**
           * One of the following:
           * - No page exists at the URL that was passed to displayDialogAsync.
           * - The page that was passed to displayDialogAsync loaded, but the dialog box was directed to a page
           * that it cannot find or load, or it has been directed to a URL with invalid syntax.
           **/
          $scope.showNotification(StringService.getValue(ConstantService.ERROR_KEY),
            StringService.getValue(ConstantService.UNKNOWN_PAGE_KEY));
          break;
        case 12003: // The dialog box was directed to a URL with the HTTP protocol. HTTPS is required.
          $scope.showNotification(StringService.getValue(ConstantService.ERROR_KEY),
            StringService.getValue(ConstantService.HTTP_PAGE_KEY));
          break;
        case 12006: // The dialog box was closed, usually because the user chooses the X button.
      }
      dialogDeny();
    }

    /**
     *
     * @param data
     */
    function processAuthenticationDialogResponse(data) {
      const messageFromPopupDialog = JSON.parse(data.message);
      if (messageFromPopupDialog.outcome === ConstantService.STATUS_SUCCESS) {
        ProfileService.activateProfile(messageFromPopupDialog, $scope.ionFile);
        resetTab();
        $scope.showNotification(StringService.getValue(ConstantService.SUCCESS_KEY),
          StringService.getValue(ConstantService.AUTH_OK_KEY));
        $timeout(function () {
          URIService.redirectToDocuments();
        });
      } else {
        $scope.showNotification(StringService.getValue(ConstantService.ERROR_KEY),
          StringService.getValue(ConstantService.AUTH_ERROR_KEY));
      }
      dialogDeny();
    }

    $scope.showContent = function (fileContent) {
      let content = JSON.parse(fileContent);

      if (content.status !== ConstantService.STATUS_ERROR) {
        $scope.content = fileContent;
        $scope.ionFile = content;
        $scope.profileName = content[ConstantService.ION_CLIENT_NAME_KEY];
        ProfileService.setDialogWindowUrl(content);
      }
    };

    function resetTab() {
      const formElement = document.getElementById('documentForm');

      formElement.reset();
      $scope.profileName = '';
    }

    $scope.init();
  }]);
