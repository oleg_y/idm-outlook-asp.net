let url;
(function () {
  'use strict';
  Office.initialize = function () {
    $(document).ready(function () {
      url = Office.context.roamingSettings.get('url');
      localStorage.messageObject = undefined;
      window.location = url;
    });
  };
})();
