/// <reference path="../app.js" />
let accessTokenForAuth0;
let messageObject;
(function () {
  "use strict";
  Office.initialize = function () {
    $(document).ready(function () {
      accessTokenForAuth0 = getHashStringParameter('access_token');
      accessTokenForAuth0 = accessTokenForAuth0.split("&")[0];
      if (accessTokenForAuth0 !== undefined) {
        messageObject = {
          outcome: "success",
          token: accessTokenForAuth0
        };
      } else {
        messageObject = {
          outcome: "failure",
          error: "Authentication failed"
        };
      }
      localStorage.messageObject = JSON.stringify(messageObject);
      jQuery("#popup")
          .text("Authenticated successfully. Due to some technical reasons,we are unable to close this window. Please close.");
      Office.context.ui.messageParent(JSON.stringify(messageObject));
    });
  };

  function getHashStringParameter(paramToRetrieve) {
    const hash = location.href.substr(1);
    const params = hash.split("#");
    for (let i = 0; i < params.length; i = i + 1) {
      const singleParam = params[i].split("=");
      if (singleParam[0] === paramToRetrieve)
        return singleParam[1];
    }
  }
}());
