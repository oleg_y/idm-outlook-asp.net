mainApp.controller('mainController', function ($scope, $location, $timeout) {
  $scope.settings = Office.context.roamingSettings;

  $scope.openLocation = function (location) {
    window.location.href = location;
  };

  $scope.initNotification = function () {
    $('body').append(
        '<div id="notification-message">' +
        '<div id="notification-message-close"></div>' +
        '<div id="notification-message-header"></div>' +
        '<div id="notification-message-body"></div>' +
        '</div>');

    $('#notification-message-close').click(function () {
      $('#notification-message').hide();
    });
  };

  $scope.showNotification = function (header, text, time) {
    if (time === undefined) {
      time = 15000;
    }
    jQuery('#notification-message-header').text(header);
    jQuery('#notification-message-body').text(text);
    jQuery('#notification-message').slideDown('fast');
    $timeout(function () {
      jQuery('#notification-message').hide();
    }, time);
  };

  $scope.hideNotification = function () {
    $('#notification-message').hide();
  };

  $scope.closeNotification = function () {
    console.log($scope.notification);
  };

  $scope.initNotification();
});
