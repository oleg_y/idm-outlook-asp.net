mainApp.factory('DocumentService', ['StringService', function (StringService) {
  let documentTypes = [];
  let selectedDocumentType = null;
  let selectedAclName = null;
  let selectedAclDescription = null;
  let selectedACLs = [];

  return {
    setDocumentTypes: function (types) {
      if (documentTypes !== undefined && documentTypes.length > 0) {
        documentTypes = types;
      }
    },
    setSelectedDocumentType: function (documentType) {
      selectedDocumentType = documentType;
    },
    getSelectedDocumentType: function () {
      return selectedDocumentType;
    },
    setSelectedACLs: function (acls) {
      if (acls !== undefined && acls.length > 0) {
        selectedACLs = acls;
      }
    },
    setSelectedACLName: function (name) {
      if (name !== undefined && name !== null) {
        selectedAclName = name;
      }
    },
    setSelectedACLDescription: function (description) {
      if (description !== undefined && description !== null) {
        selectedAclDescription = description;
      }
    },
    getSelectedACLName: function () {
      return selectedAclName;
    },
    filterDocumentTypesLength: function (documentTypes) {
      for (let i = 0, len = documentTypes.length; i < len; i++) {
        documentTypes[i].shortDesc = StringService.truncateString(documentTypes[i].desc);
      }
      return documentTypes;
    }
  };
}]);