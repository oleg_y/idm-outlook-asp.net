mainApp.factory('AttachmentsService', ['$q', '$http', 'URIService', 'StringService',
  function ($q, $http, URIService, StringService) {
    const officeContext = Office.context;
    let selectedAttachment = null;

    return {
      getAttachments: function () {
        return officeContext.mailbox.item.attachments.filter(function (attachment) {
          return attachment.isInline === false && attachment.size > 0;
        });
      },
      setSelectedAttachment: function (attachment) {
        if (attachment !== undefined && attachment !== null) {
          selectedAttachment = attachment;
        }
      },
      getSelectedAttachment: function () {
        if (selectedAttachment === null) {
          return this.getAttachments()[0];
        }
        return selectedAttachment;
      },
      getEWSAttachmentRequest: function (ewsToken) {
        const attachment = this.getSelectedAttachment();
        const attachments = [{
          AttachmentType: attachment.attachmentType,
          ContentType: attachment.contentType,
          Id: attachment.id,
          IsInline: attachment.isInline,
          Name: attachment.name,
          Size: attachment.size
        }];
        return {
          method: URIService.HTTPMethods.POST,
          url: URIService.APP_PATHS.ATTACHMENTS,
          data: JSON.stringify({
            "AttachmentToken": ewsToken,
            "EwsUrl": officeContext.mailbox.ewsUrl,
            "AttachmentName": attachment.name,
            "AttachmentId": attachment.id
          }),
          contentType: 'application/json;charset=utf-8',
          timeout: $q.defer().promise
        };
      },
      filterAttachmentsNameLength: function (attachments) {
        for (let i = 0, len = attachments.length; i < len; i++) {
          attachments[i].shortName = StringService.truncateString(attachments[i].name);
        }
        return attachments;
      }
    };
  }]);