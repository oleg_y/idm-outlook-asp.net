mainApp.factory('ItemService', ['$q', '$http', 'DocumentService', 'AttachmentsService', 'ProfileService', 'URIService', 'ConstantService',
  function ($q, $http, DocumentService, AttachmentsService, ProfileService, URIService, ConstantService) {

    const getPostPayload = function (ewsAttachment) {
      const selectedDocumentType = DocumentService.getSelectedDocumentType();
      let updatedAttributes = [];

      if (selectedDocumentType !== undefined && selectedDocumentType !== null) {
        updatedAttributes = selectedDocumentType.attrs.attr.filter(function (attribute) {
          return attribute.value !== undefined;
        });
      }
      return {
        "item": {
          "acl": {
            "name": DocumentService.getSelectedACLName()
          },
          "attrs": {
            "attr": updatedAttributes.map(function (attribute) {
              return {
                name: attribute.name,
                value: attribute.value
              };
            })
          },
          "resrs": {
            "res": [
              {
                "filename": ewsAttachment.name,
                "base64": ewsAttachment.content
              }
            ]
          },
          "entityName": selectedDocumentType === null ? '' : selectedDocumentType.name
        }
      };
    };

    return {
      getEWSToken: function (callback) {
        return Office.context.mailbox.getCallbackTokenAsync(callback);
      },
      retrieveAttachment: function (ewsToken) {
        return $http(AttachmentsService.getEWSAttachmentRequest(ewsToken));
      },
      postToIDM: function (ewsAttachment) {
        const activeProfile = ProfileService.getActiveProfile();
        const request = {
          method: URIService.HTTPMethods.POST,
          url: URIService.getItemsPostEndpoint(activeProfile),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + activeProfile[ConstantService.TOKEN_KEY]
          },
          data: getPostPayload(ewsAttachment),
          timeout: $q.defer().promise
        };
        return $http(request);
      }
    };
  }]);