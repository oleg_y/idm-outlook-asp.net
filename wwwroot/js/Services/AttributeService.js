mainApp.factory('AttributeService', ['$log', 'StringService', 'ConstantService',
  function ($log, StringService, ConstantService) {
    const TIME_FORMATS = {
      DATE: 'YYYY-MM-DD',
      DATETIME: 'YYYY-MM-DD hh:mm:ss',
      TIME: 'hh:mm:ss'
    };

    return {
      ATTRIBUTE_TYPES: {
        STRING: '1',
        SHORT: '3',
        LONG: '4',
        DECIMAL: '6',
        DATE: '7',
        TIME: '8',
        TIMESTAMP: '9',
        DOUBLE: '10',
        BOOLEAN: '20',
        GUID: '21'
      },
      hasValidBoolean: function (attribute) {
        if (!this.isAttributeValueEmpty(attribute, 'value')) {
          return ['true', 'false'].indexOf(attribute.value) !== -1;
        }
      },
      hasValidTime: function (attribute) {
        if (!this.isAttributeValueEmpty(attribute, 'value')) {
          let timeValue = attribute.value;
          if (attribute.value.format !== undefined) {
            timeValue = attribute.value.format(TIME_FORMATS.TIME);
          }
          return /^\d{2}:\d{2}:\d{2}$/.test(timeValue);
        }
      },
      hasValidDate: function (attribute) {
        if (!this.isAttributeValueEmpty(attribute, 'value')) {
          let dateValue = attribute.value;
          if (attribute.value.format !== undefined) {
            dateValue = attribute.value.format(TIME_FORMATS.DATE);
          }
          return /^\d{4}-\d{2}-\d{2}$/.test(dateValue);
        }
      },
      hasValidTimestamp: function (attribute) {
        if (!this.isAttributeValueEmpty(attribute, 'value')) {
          let timestampValue = attribute.value;
          if (attribute.value.format !== undefined) {
            timestampValue = attribute.value.format(TIME_FORMATS.DATETIME);
          }
          return /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/.test(timestampValue);
        }
      },
      hasValidStringLimit: function (attribute) {
        try {
          return Number(attribute.size) > attribute.value.length;
        } catch (e) {
          $log.debug('Error validating string size: ', e.message);
          return false;
        }
      },
      hasValidNumber: function(attribute) {
        return /^[0-9.,]+$/.test(attribute.value);
      },
      hasValidMinNumber: function (attribute) {
        return Number(attribute.value) >= Number(attribute.min);
      },
      hasValidMaxNumber: function (attribute) {
        return Number(attribute.value) <= Number(attribute.max);
      },
      isAttributeValueEmpty: function (attribute, key) {
        if (attribute[key] === undefined) {
          return true;
        }
        return attribute[key].toString().replace(/\s+/, '') === '';
      },
      isAttributeRequired: function (attribute) {
        return attribute.required === 'true';
      },
      isAttributeTypeBoolean: function (attribute) {
        return attribute.type === this.ATTRIBUTE_TYPES.BOOLEAN;
      },
      isAttributeTypeDate: function (attribute) {
        return attribute.type === this.ATTRIBUTE_TYPES.DATE;
      },
      isAttributeTypeTime: function (attribute) {
        return attribute.type === this.ATTRIBUTE_TYPES.TIME;
      },
      isAttributeTypeTimestamp: function (attribute) {
        return attribute.type === this.ATTRIBUTE_TYPES.TIMESTAMP;
      },
      isAttributeTypeValueSet: function (attribute) {
        try {
          // noinspection JSUnresolvedVariable
          return attribute.valueset.value.length > 0;
        } catch (e) {
          return false;
        }
      },
      isAttributeTypeLimitedString: function (attribute) {
        return attribute.type === this.ATTRIBUTE_TYPES.STRING
          && !this.isAttributeValueEmpty(attribute, 'size')
          && !this.isAttributeTypeValueSet(attribute);
      },
      hasAttributeMinNumber: function (attribute) {
        const min = Number(attribute.min);
        return !isNaN(min) && min > 0;
      },
      hasAttributeMaxNumber: function (attribute) {
        const max = Number(attribute.max);
        return !isNaN(max) && max > 0;
      },
      getAttributeSize: function (attribute) {
        return attribute.size === undefined ? '0' : attribute.size;
      },

      validateBoolean: function (attribute) {
        attribute.valid = this.hasValidBoolean(attribute);
        if (attribute.valid !== false) {
          attribute.validationError = StringService.getValue(ConstantService.INVALID_VALUE_KEY);
        }
      },
      validateDate: function (attribute) {
        attribute.valid = this.hasValidDate(attribute);
        if (attribute.valid === false) {
          attribute.validationError = StringService.getValue(ConstantService.INVALID_DATE_KEY);
        }
      },
      validateTime: function (attribute) {
        attribute.valid = this.hasValidTime(attribute);
        if (attribute.valid === false) {
          attribute.validationError = StringService.getValue(ConstantService.INVALID_TIME_KEY);
        }
      },
      validateTimestamp: function (attribute) {
        attribute.valid = this.hasValidTimestamp(attribute);
        if (attribute.valid === false) {
          attribute.validationError = StringService.getValue(ConstantService.INVALID_TIMESTAMP_KEY);
        }
      },
      validateString: function (attribute) {
        attribute.valid = this.hasValidStringLimit(attribute);
        if (attribute.valid === false) {
          attribute.validationError = StringService.getValue(
            ConstantService.MAX_LENGTH_KEY, this.getAttributeSize(attribute));
        }
      },
      validateNumber: function(attribute) {
        attribute.valid = this.hasValidNumber(attribute);
        if (attribute.valid === false) {
          attribute.validationError = StringService.getValue(ConstantService.INVALID_NUMBER_KEY);
          return;
        }
        if (this.hasAttributeMinNumber(attribute)) {
          this.validateMinNumber(attribute);
        }
        if (attribute.valid !== false && this.hasAttributeMaxNumber(attribute)) {
          this.validateMaxNumber(attribute);
        }
      },
      validateMinNumber: function (attribute) {
        attribute.valid = this.hasValidMinNumber(attribute);
        if (attribute.valid === false) {
          attribute.validationError = StringService.getValue(ConstantService.MIN_NUMBER_KEY, attribute.min);
        }
      },
      validateMaxNumber: function (attribute) {
        attribute.valid = this.hasValidMaxNumber(attribute);
        if (attribute.valid === false) {
          attribute.validationError = StringService.getValue(ConstantService.MAX_NUMBER_KEY, attribute.max);
        }
      },
      resetAttributes: function (attributes) {
        if (attributes !== undefined) {
          attributes.forEach(function (attribute) {
            delete attribute.value;
          });
        }
      }
    };
  }]);