mainApp.factory('URIService', ['$window', '$q', '$location', 'ConstantService',
  function ($window, $q, $location, ConstantService) {

    const getDocumentTypesEndpoint = function (activeProfile) {
      const host = activeProfile[ConstantService.ION_OBJECT_KEY][ConstantService.ION_OBJECT_HOST_KEY];
      const path = activeProfile[ConstantService.ION_OBJECT_KEY][ConstantService.ION_OBJECT_PATH_KEY];
      return host + '/' + path + '/IDM/api/datamodel/entities';
    };

    const HTTP_PORTS = {
      SSL_PROTOCOL: 443
    };

    return {
      HTTPMethods: {
        GET: 'GET',
        POST: 'POST'
      },
      HTTPStatuses: {
        INVALID_CLIENT: 400,
        UNAUTHORIZED: 401,
        SERVER_ERROR: 500,
        UNKNOWN: -1
      },
      APP_PATHS: {
        ATTACHMENTS: '/api/attachment',
        DOCUMENTS: '#/document',
        SETTINGS: '#/settings'
      },
      getAppUrl: function() {
        let url = $location.protocol() + '://' + $location.host();
        const port = Number($location.port());
        if (!isNaN(port) && port > 0 && port !== HTTP_PORTS.SSL_PROTOCOL) {
          url += ':' + port;
        }
        return url;
      },
      getDialogWindowUrl: function () {
        return this.getAppUrl() + '/Home/Dialog';
      },
      getRedirectUrl: function() {
        return this.getAppUrl() + '/Home/AuthRedirect';
      },
      getDocumentTypesRequest: function (activeProfile) {
        return {
          method: this.HTTPMethods.GET,
          url: getDocumentTypesEndpoint(activeProfile),
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + activeProfile[ConstantService.TOKEN_KEY]
          }
        };
      },
      getItemsPostEndpoint: function (activeProfile) {
        if (activeProfile !== null) {
          const host = activeProfile[ConstantService.ION_OBJECT_KEY][ConstantService.ION_OBJECT_HOST_KEY];
          const path = activeProfile[ConstantService.ION_OBJECT_KEY][ConstantService.ION_OBJECT_PATH_KEY];
          return host + '/' + path + '/IDM/api/items';
        }
        return null;
      },
      redirectToSettings: function () {
        $window.location.href = this.APP_PATHS.SETTINGS;
      },
      redirectToDocuments: function () {
        $window.location.href = this.APP_PATHS.DOCUMENTS;
      }
    };
  }]);