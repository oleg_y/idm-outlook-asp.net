mainApp.factory('ProfileService', ['$log', '$location', 'ConstantService', 'URIService', function ($log, $location, ConstantService, URIService) {

  const settings = Office.context.roamingSettings;

  return {

    getProfiles: function () {
      let profiles = settings.get(ConstantService.PROFILES_KEY);
      if (profiles === undefined || profiles.length === 0) {
        profiles = JSON.parse(localStorage.getItem(ConstantService.PROFILES_KEY));
      }
      if (profiles === undefined || profiles === null) {
        return [];
      }
      return profiles;
    },

    getActiveProfile: function () {
      let activeProfile = settings.get(ConstantService.ACTIVE_PROFILE_KEY);
      if (activeProfile === undefined) {
        activeProfile = JSON.parse(localStorage.getItem(ConstantService.ACTIVE_PROFILE_KEY));
      }
      return activeProfile;
    },

    setProfile: function (profile) {
      settings.set(ConstantService.NAME_KEY, profile.name);
      settings.set(ConstantService.ION_OBJECT_KEY, profile[ConstantService.ION_OBJECT_KEY]);
      settings.saveAsync();

      localStorage.setItem(ConstantService.NAME_KEY, profile.name);
      localStorage.setItem(ConstantService.ION_OBJECT_KEY, JSON.stringify(profile[ConstantService.ION_OBJECT_KEY]));

      this.setDialogWindowUrl(profile[ConstantService.ION_OBJECT_KEY]);
    },

    setProfiles: function (profiles, activeProfile) {
      settings.set(ConstantService.PROFILES_KEY, profiles);
      localStorage.setItem(ConstantService.PROFILES_KEY, JSON.stringify(profiles));

      if (activeProfile !== undefined) {
        settings.set(ConstantService.ACTIVE_PROFILE_KEY, activeProfile);
        localStorage.setItem(ConstantService.ACTIVE_PROFILE_KEY, JSON.stringify(activeProfile));
      }
      settings.saveAsync();
    },

    setDialogWindowUrl: function (authFileContent) {
      const url = authFileContent[ConstantService.ION_REFRESH_HOST_KEY] +
        authFileContent[ConstantService.ION_AUTH_PATH] + '?client_id=' +
        authFileContent[ConstantService.ION_CLIENT_ID_KEY] + '&response_type=token&redirect_uri=' +
        URIService.getRedirectUrl();
      settings.set(ConstantService.URL_KEY, url);
      settings.saveAsync();
      localStorage.setItem(ConstantService.URL_KEY, url);
    },

    /**
     * Remove profile from both settings and local storage
     * @param profile Authentication profile
     */
    removeProfile: function (profile) {
      const profiles = this.getProfiles().filter(function (currentProfile) {
        return currentProfile.name !== profile.name;
      });
      this.setProfiles(profiles);
      this.removeActiveProfile(profile);
    },

    /**
     * Remove active profile from both settings and local storage
     * @param profile Authentication profile
     */
    removeActiveProfile: function (profile) {
      const activeProfile = this.getActiveProfile();
      if (activeProfile !== null && activeProfile.name === profile.name) {
        settings.remove(ConstantService.ACTIVE_PROFILE_KEY);
        localStorage.removeItem(ConstantService.ACTIVE_PROFILE_KEY);
      }
    },

    /**
     * Activating profile, consists of:
     * 1. If profile with this name was previously activated, remove old profile
     * 2. Add new profile to the profiles collection
     * 3. Save new profile as active profile
     * @param messageFromPopupDialog JSON message returned from Microsoft Office dialog
     * @param ionFile ION API authentication file
     */
    activateProfile: function (messageFromPopupDialog, ionFile) {
      const profiles = this.getProfiles().filter(function (profile) {
        return profile[ConstantService.NAME_KEY] !== ionFile[ConstantService.ION_CLIENT_NAME_KEY];
      });
      messageFromPopupDialog[ConstantService.NAME_KEY] = ionFile[ConstantService.ION_CLIENT_NAME_KEY];
      messageFromPopupDialog[ConstantService.ION_OBJECT_KEY] = ionFile;
      profiles.push(messageFromPopupDialog);
      this.setProfiles(profiles, messageFromPopupDialog);
    }
  };
}]);