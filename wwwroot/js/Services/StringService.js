mainApp.factory('StringService', ['ConstantService', function (ConstantService) {
  return {
    ADD_PROFILE: 'Please add your Profile',
    ATTACHMENTS_EMPTY_ERROR: 'No attachments found in this conversation. Please choose another one.',
    AUTH_ERROR: 'Authentication error.',
    AUTH_OK: 'Authenticated successfully.',
    CONFIGURATION_ERROR: 'Configuration Issue',
    CONNECT_PROFILE: 'Please connect with attached profile.',
    ERROR: 'Error',
    HTTP_PAGE: 'The dialog box was directed to a URL with the HTTP protocol. HTTPS is required.',
    INVALID_DATE: 'Enter value in date format YYYY-MM-DD.',
    INVALID_NUMBER: 'Invalid number.',
    INVALID_TIME: 'Enter value in time format hh:mm:ss.',
    INVALID_TIMESTAMP: 'Enter value in date format YYYY-MM-DD hh:mm:ss.',
    INVALID_VALUE: 'Invalid value.',
    MAX_LENGTH: 'Max length should be %s',
    MAX_NUMBER: 'Number should be equal or less than %s',
    MIN_NUMBER: 'Number should be equal or greater than %s',
    NOT_SUPPORTED: 'Not Supported',
    PROFILE_EXPIRED: 'Your profile has expired. Please authenticate with a new profile.',
    PROFILE_NAME_EMPTY: 'Profile name should not be empty.',
    PROFILE_NAME_EXISTS: 'Profile name already exists. Please try another name.',
    PROFILE_REMOVED: 'Profile %s was removed',
    PROFILES_EMPTY_ERROR: 'No profiles found. Please configure one to continue.',
    REQUIRED_FIELD: 'This field is required.',
    SELECT_ACL: 'Please select ACL',
    SERVER_ERROR: 'Internal Server Error, please try again later.',
    SUCCESS: 'Success',
    UNKNOWN_PAGE: 'The dialog box has been directed to a page that it cannot find or load, or the URL syntax is invalid.',
    UPDATE_OUTLOOK: 'Please update your outlook.',
    UPLOAD_ION_FILE: 'Please upload a valid ionAPI file.',

    getValue: function (key, placeholder) {
      if (key in this) {
        let value = this[key];
        return placeholder === undefined ? value : value.replace('%s', placeholder);
      }
      return null;
    },

    truncateString: function (value) {
      const maxLength = ConstantService.MAX_ELEMENT_STRING_LENGTH;
      let truncatedString = value.substring(0, maxLength);
      if (value.length > maxLength) {
        truncatedString += '...';
      }
      return truncatedString;
    }
  };
}]);